import {request} from '../api'
export const SERVICE = 'http://localhost:3000/api'

export const getCars = async ()=> {
    const {error, result} = await request('POST', SERVICE + '/getCars')
    return {error, result}
}

export const addCar = async (cars)=> {
    const {error, result} = await request('POST', SERVICE + '/addCar', cars)
    return {error, result}
}

export const editCar = async (car, id) => {
    const {error, result} = await request('POST', SERVICE + '/editCar/' + id, car)
    return {error, result}
}

export const deleteCar = async (id) => {
    const {error, result} = await request('POST', SERVICE + '/deleteCar/' + id)
    return {error, result}
}

export const searchCar = async (searchData) => {
    const {error, result} = await request('POST', SERVICE + '/getCars/' + '?search=' + searchData)
    return {error, result}
}

