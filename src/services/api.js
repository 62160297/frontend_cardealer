import axios from "axios";

const API = axios.create({
    baseURL: 'http://localhost:3000/api',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
    }
})


const handleResponseData = (resp) => resp.data;
const catchErrorResponse = (err, callback) => {
    let error = err.response.status;
    if (error === 403) {
      callback(err.response);
    } else if (error === 401){
      callback(err.response);
    }else {
      callback(err.response);
    }
  };

export const request = async (type, path, data, responseType) => {
    let error,result;
    var resp;
    try {
        switch (type) {
            case "GET":
                resp = await API.get(path, { params: data });
                break;
            case "POST":
                resp = await API.post(path, data, { responseType: responseType })
                    .then()
                    .catch();
                break;
            case "PUT":
                resp = await API.put(path, data);
                break;
            case "DELETE":
                resp = await API.delept(path, { path, data });
                break;
            default:
                resp = await API.get(path, { params: data });
                break;
        }
        if (!resp) {
            error = resp;
            result = null;
        }
        result = handleResponseData(resp);
        return {
            error: null,
            result,
        };

    } catch(err){
        catchErrorResponse(err, (e) => {
            if (e.status === 400 || e.status === 500){
                error = e;
                result = e.data;
            } else{
                error = e;
            }
        });
        return {
            error,
            result,
        };
    }
};

export default API;
