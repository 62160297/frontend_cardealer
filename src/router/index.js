import Vue from 'vue'
import Router from "vue-router";

Vue.use(Router);

const router = new Router({
  mode: "history",
  routes: configRoutes(),
});

function configRoutes() {
  return [
    {
      path: '/',
      name: 'CarPage',
      component: () => import('@/views/carPage.vue')
    }
  ]
}

export default router
