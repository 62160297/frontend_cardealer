export function isLetter(event){
        let char = String.fromCharCode(event.keyCode); // รับค่าตัวอักษร
    if (/^[A-Za-z0-9- ]+$/.test(char)) return true;
    // ตั้งค่าเงื่อนไข
    else event.preventDefault(); // ถ้าเกิดว่า input ค่าที่รับเข้ามาไม่ตรงกับเงื่อนไขจะไม่ทำงาน
}

export function isLetterNum(event) {
    let char = String.fromCharCode(event.keyCode); // รับค่าตัวอักษร
    if (/^[0-9]+$/.test(char)) return true;
    // ตั้งค่าเงื่อนไข
    else event.preventDefault(); // ถ้าเกิดว่า input ค่าที่รับเข้ามาไม่ตรงกับเงื่อนไขจะไม่ทำงาน
}

export function isLetterCha(event) {
    let char = String.fromCharCode(event.keyCode); // รับค่าตัวอักษร
    if (/^[A-Za-z]+$/.test(char)) return true;
    // ตั้งค่าเงื่อนไข
    else event.preventDefault(); // ถ้าเกิดว่า input ค่าที่รับเข้ามาไม่ตรงกับเงื่อนไขจะไม่ทำงาน
}
